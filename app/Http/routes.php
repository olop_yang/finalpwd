<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/',['as' => 'game.index', 'uses' => 'GameController@index']);

Route::get('two',['as' => 'gametwo.index', 'uses' => 'GameTwoController@index']);

//Route::get('/dd',['as' => 'game.destory', 'uses' => 'GameController@destory']);

