<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Game;
use App\SessionData;
use App\Exceptions\GameException;

class GameTwoController extends Controller
{
   //
    protected $game;



    public function index(Request $request)
    {
        $ans = $request->input('ans');

        $this->game = new Game(new SessionData());

        $data = $this->game->getNums();

        if (!isset($ans)) {
            $data = $this->game->init();
            return view('gametwo', compact('data'));
        }

        if (!ctype_digit($ans)) {
            $msg['error'] = '請輸入整數';
            return view('gametwo', compact('data', 'msg'));
        }

        if ($this->game->isMatch($ans)) {

            $data = $this->game->addCount();
            //$this->sessionData->dealData($count);

            $msg = ['final' => 'Game Over', 'restart' => '重新開始遊戲'];

            //$this->game->setNums(session()->all());
            //$data = $this->game->getNums();
            return view('gametwo', compact('data', 'msg'));              
        }

        if (!$this->game->checkMin($ans)) {
            $msg['error']= '答案需大於' . $data['min'];
            return view('gametwo', compact('data', 'msg'));
        }

        if (!$this->game->checkMax($ans)) {
            $msg['error']= '答案需小於' . $data['max'];
            return view('gametwo', compact('data', 'msg'));
        }
        
        $this->game->addCount();
        $data = $this->game->setMinMax($ans);  
        $msg = $this->game->checkCount();

        return view('gametwo', compact('data', 'msg'));

    }


    public function backup()
    {
            //重新開始，沒有值
            if (false == $this->game->isSetAns($ans)) {
                session()->flush();
                session(['pwd' => rand(1,100)]);
                session(['min' => 1]);
                session(['max' => 100]);
                session(['count' => 0]);

            //有值，然後檢查是不是整數
            }elseif (true == $this->game->isIntAns($ans)) {

                //是整數確認match
                if (true == $this->game->isMatch($ans, session('pwd'))) {

                    session(['count' => $this->game->addCount(session('count'))]);
                    $this->game->setPassOrFail(false);

                } elseif ('pass' == 
                            $this->game->checkInRange($ans, session('min'), session('max'))) { 
                        session(['min' => 
                                    $this->game->setMin($ans, session('min'), session('pwd'))]);
                        session(['max' => 
                                    $this->game->setMax($ans, session('max'), session('pwd'))]);    
                        session(['count' => $this->game->addCount(session('count'))]);  
                        $this->game->checkCount(session('count'));

                }
                    //不在範圍裡面 
            } else {  //not int
      
                $this->game->setError('notint');
            }


            $data = session()->all();
            $msg = $this->game->getMsg();
            //dd($msg);
            return view('gametwo', compact('data','msg'));    
    }



    public function validGame($ans)
    {
        if (!isset($ans)){
           throw new Exception("Error Processing Request", 1);             
        }

        if (!$this->isIntAns($ans)){

        }
    }

    public function destory(Request $request)
    {
        $request->session()->flush();

        return Redirect::to('/');
    }
}
