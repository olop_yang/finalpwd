<?php
namespace App;

interface GameType
{
    public function init();

    public function getAll();

    public function deal($data = array());
}