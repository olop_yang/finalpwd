<?php

namespace App;

class Game 
{
    protected $nums = [];

    protected $gameType;

    public function __construct(GameType $data)
    {
        $this->gameType = $data;

        $this->setNums($this->gameType->getAll());
    }
    public function init()
    {
        return $nums = $this->gameType->init();
    }

    public function setNums($data = array())
    {
        foreach ($data as $key => $value) {
            $this->nums[$key] = $value;
        }

        return $this->nums;
    }

    public function getNums()
    {
        return $this->nums;
    }


    public function isMatch($ans)
    {
        if ($ans == $this->nums['pwd']) {
            return true;
        }
        return false;
    }


    public function checkMin($ans)
    {
        if ($ans <= $this->nums['min']) {
            return false;
        }
        return true;
    }

    public function checkMax($ans)
    {
        if ($ans >= $this->nums['max']) {
            return false;
        }
        return true;
    }

    public function setMinMax($ans)
    {
        if ($ans < $this->nums['pwd']) {
            $this->nums['min'] = $ans;
        } else {
            $this->nums['max'] = $ans;
        }

        return $this->gameType->deal($this->nums);
    }

    public function addCount()
    {
        $this->nums['count'] = $this->nums['count']+1;

        return $this->gameType->deal($this->nums);
    }

    public function checkCount()
    {
        if ( $this->nums['count'] == 5 ) {
            return $msg = ['final' => '恭喜您過關', 'restart' => '重新開始遊戲'];
        } else {
            return null;
        }
    }
 

}
