<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Game;
use App\StubData;
class GameTest extends TestCase
{

    protected $game;

    public function testInit()
    {
        $stub = [   'pwd' => 6, 
                    'min' => 2,
                    'max' =>99,
                    'count' => 0]; 
        $this->game = new Game(new StubData());
        $nums = $this->game->init();
        foreach ($nums as $key => $value) {
            $this->assertEquals($stub[$key], $nums[$key]);
        }
    }

   public function testIsMatch()
    {

        $this->game = new Game(new StubData());
        $this->assertFalse($this->game->isMatch(100));
        $this->assertTrue($this->game->isMatch(10));
    }

    public function testAddCount()
    {
        $this->game = new Game(new StubData());
        $nums = $this->game->addCount();
        $this->assertEquals(6, $nums['count']);
    }

    public function testCheckMin()
    {
        $this->game = new Game(new StubData());

        $this->assertFalse($this->game->checkMin(0));
        $this->assertTrue($this->game->checkMin(101));
    }

    public function testCheckMax()
    {
        $this->game = new Game(new StubData());
        
        $this->assertFalse($this->game->checkMax(101));
        $this->assertTrue($this->game->checkMax(77));
    }

    public function testSetMinMax()
    {
        $this->game = new Game(new StubData());

        $nums1 = $this->game->setMinMax(7);
        $nums2 = $this->game->setMinMax(70);

        $this->assertEquals(7, $nums1['min']);
        $this->assertEquals(70, $nums2['max']);
    }

    public function testCheckCount()
    {
        $this->game = new Game(new StubData());
        $msg = $this->game->checkCount();
        $this->assertEquals(['final' => '恭喜您過關', 'restart' => '重新開始遊戲'], $msg);        
    }

}