<!DOCTYPE html>
<html>
    <head>
        <title>Game</title>

    </head>
    <body>
        <div class="container">
            <div style= "text-align:center">
                <p>{{ $data['pwd'] }}</p>

                <form method="GET" action="/">
                    <p>請輸入{{ $data['min'] }} ~ {{ $data['max'] }}的數字</p>
                    請輸入答案
                    <input type="text" name="ans">
                    <button type="submit">送出</button>
                    <p>這是您{{ $data['count'] }}第次回答</p>
                    <?php if(isset($error)):?>
                        <p>{{ $error }}</p>
                    <?php endif;?>
                </form>

                <?php if (isset($data['msg'])):?>       
                <p>{{ $data['msg'] }}</p>
                <?php endif;?>

                <?php if (isset($data['msg'])):?>
                <a href="/">
                    <button type ="submit">{{ $data['restart'] }}</button>
                </a>
                <?php endif;?>  
            </div>
        </div>
    </body>
</html>
