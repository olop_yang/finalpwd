<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\GameTwo;
class GameTest extends TestCase
{

    protected $game;


    public function testInit()
    {
        $this->game = new GameTwo();

        $this->assertEquals()
    }

    public function testIsSetAns()
    {
        $this->game = new GameTwo();
        $this->assertFalse($this->game->isSetAns(null));

        $this->assertTrue($this->game->isSetAns('a'));

    }

    public function testIsIntAns()
    {
        $notInts = [22.5, 'a', '*', ''];

        $this->game = new GameTwo();
        foreach ($notInts as $notInt) {
            $this->assertFalse($this->game->isIntAns($notInt));         
        }

    }

    public function testIsMatch()
    {
        $this->game = new GameTwo();
        $this->assertFalse($this->game->isMatch(5, 8));
        $this->assertTrue($this->game->isMatch(5, 5));

    }

    public function testCheckInRange()
    {
        $this->game = new GameTwo();

        $this->assertEquals('pass', $this->game->checkInRange(15, 12, 60));
        $this->assertEquals('數字需大於12', $this->game->checkInRange(12, 12, 60));
        $this->assertEquals('數字需小於60', $this->game->checkInRange(60, 12, 60));
    }

    public function testSetMin()
    {
        $this->game = new GameTwo();
        $this->assertEquals(45, $this->game->setMin(45, 40, 50));
        $this->assertEquals(40, $this->game->setMin(60, 40, 50));

    }

    public function testSetMax()
    {
        $this->game = new GameTwo();
        $this->assertEquals(70, $this->game->setMax(70, 90, 60));
        $this->assertEquals(90, $this->game->setMax(40, 90, 60));

    }

    public function testCheckCount()
    {
        $this->game = new GameTwo();
        $this->assertEquals('恭喜您過關', $this->game->checkCount(10));
        $this->assertEquals(null, $this->game->checkCount(2));        
    }

    public function testSetPassOrFail()
    {
        $this->game = new GameTwo();
        $this->assertEquals('恭喜您過關', $this->game->setPassOrFail(true));
        $this->assertEquals('GAME OVER', $this->game->setPassOrFail(false));          
    }

    public function testSetRestart()
    {
        $this->game = new GameTwo();
        $this->assertEquals('重新開始遊戲', $this->game->setRestart());
    }

    public function testSetError()
    {       
        $this->game = new GameTwo();
        $this->assertEquals('請輸入整數', $this->game->setError('notint'));
        $this->assertEquals('數字需大於55', $this->game->setError('less',55));
        $this->assertEquals('數字需小於55', $this->game->setError('more', null, 55 ));
    }

    public function testAddCount()
    {
        $this->game = new GameTwo();
        $num = 7;
        $this->assertEquals($num + 1, $this->game->addCount($num));
    }

}