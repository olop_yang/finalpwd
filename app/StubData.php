<?php

namespace App;

use App\GameType;
use Illuminate\Database\Eloquent\Model;

class StubData extends Model implements GameType
{
    //

    public function init()
    {
        $stub = [   'pwd' => 6, 
                    'min' => 2,
                    'max' =>99,
                    'count' => 0]; 

	    return $stub;
    }

    public function getAll()
    {
        $stub = [   'pwd' => 10, 
                    'min' => 3,
                    'max' =>88,
                    'count' => 5];

        return $stub;
    }

    public function deal($data = array())
    {
        $stub = [];
    
    	foreach ($data as $key => $value) {
    		$stub[$key] = $value;
    	}

        return $stub;
    }
}
