<?php

namespace App;

use App\GameType;
use Illuminate\Database\Eloquent\Model;

class SessionData extends Model implements GameType
{
    //

    public function init()
    {
	    session()->flush();
	    session(['pwd' => rand(1,100)]);
	    session(['min' => 1]);
	    session(['max' => 100]);
	    session(['count' => 0]);

	    return session()->all();
    }

    public function getAll()
    {
        return session()->all();
    }

    public function deal($data = array())
    {
    	foreach ($data as $key => $value) {
    		session([$key => $value]);
    	}

        return session()->all();
    }
}
