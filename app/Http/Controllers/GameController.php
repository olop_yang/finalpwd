<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Redirect,Session;
use App\Game;

class GameController extends Controller
{
    //
    protected $game;

    public function __construct()
    {

    }

    public function index(Request $request)
    {
        
        //dd(session()->all()); 
        $this->game = new Game($request->input('ans'));
        
        $data = session()->all();
        $error = $this->game->getError();
        
        return view('game', compact('data','error'));
    }

    public function destory(Request $request)
    {
        $request->session()->flush();

        return Redirect::to('/');
    }
}
