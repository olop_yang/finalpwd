<!DOCTYPE html>
<html>
    <head>
        <title>Game</title>

    </head>
    <body>
        <div class="container">
            <div style= "text-align:center">
               <p>{{ $data['pwd'] }}</p> 

                <form method="GET" action="/two">
                    <p>請輸入{{ $data['min'] }} ~ {{ $data['max'] }}的數字</p>
                    請輸入答案
                    <input type="text" name="ans">
                    <button type="submit">送出</button>
                    <p>這是您{{ $data['count'] }}第次回答</p>

                    @if (isset($msg['error']))
                        <p>{{ $msg['error'] }}</p>
                    @endif
                </form>

                @if (isset($msg['final']))      
                <p>{{ $msg['final'] }}</p>
                <a href="/two">
                    <button type ="submit">{{ $msg['restart'] }}</button>
                </a>
                @endif
            </div>
        </div>
    </body>
</html>
